<?php

namespace App\Http\Controllers;

use App\desired_item;
use App\category;
use Illuminate\Http\Request;

class DesiredItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itemsList = desired_item::All();
        $categorysList = category::All();
        foreach($itemsList as $item)
        {
            $item->category = $item->categorys->name;
            $item->save();
        }


        return view('desired_item.index', ['items'=>$itemsList, 'categorys' =>$categorysList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('desired_item.create_item');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //desired_item::create($request->all());
        $item = new desired_item;
        $name = $item->name = $request->input('name');
        //$item->date = getdate();


        $item->link = $request->input('link');
        $item->description = $request->input('description');
        $item->id_category = $request->input('category');

        $item->save();

        if($request->hasFile('picture'))
            if($request->file('picture')->isValid()) {
                $file=$request->file('picture');
                //$path = $file->path();
                $extension = $file->extension();
                $file_name=$name.$item->id.'.'.$extension;
                $directory_counter = $item->id%99+1;
                $directory_counter2 = rand ( 1, 99 );
                $path=public_path() ."/images/".$directory_counter.'/'.$directory_counter2."/";
                //$path= desired_item::checkLink(public_path() ."/images/"."1/");

                $item->picture="/images/".$directory_counter.'/'.$directory_counter2."/".$file_name;
                $file->move($path, $file_name);
            }
        $item->save();
        return redirect('desired_item');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\desired_item  $desired_item
     * @return \Illuminate\Http\Response
     */
    public function show(desired_item $desired_item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\desired_item  $desired_item
     * @return \Illuminate\Http\Response
     */
    public function edit(desired_item $desired_item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\desired_item  $desired_item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, desired_item $desired_item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\desired_item  $desired_item
     * @return \Illuminate\Http\Response
     */
    public function destroy(desired_item $desired_item)
    {
        //
    }
}
