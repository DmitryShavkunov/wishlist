<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    public $timestamps = false;
    public function d_items()
    {
        return $this->hasMany('App\desired_item', 'id_category');
    }
}
