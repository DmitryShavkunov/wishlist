<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/desired_item','DesiredItemController@index');
Route::get('/desired_item/create_item.php','DesiredItemController@create');
Route::post('/desired_item/store.php','DesiredItemController@store');
