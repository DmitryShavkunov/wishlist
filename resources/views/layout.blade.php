<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--(html comment removed:  The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags )-->
    <title>Phalcon PHP Framework</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

@include('layouts.nav')
<div class="container">

    @yield('content')

</div>

<!--(html comment removed:  jQuery (necessary for Bootstrap's JavaScript plugins) )-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!--(html comment removed:  Latest compiled and minified JavaScript )-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>