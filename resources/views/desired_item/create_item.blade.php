
@extends ('layout')

@section ('content')
    <div class="page-header">
        <h1>
            Создание предмета
        </h1>
    </div>
    <h3>Создайте предмет для вашего списка желаемого.</h3>
    <div class="input-group">
        <form method="POST" action="store.php" enctype="multipart/form-data">
        {{ csrf_field() }}
            <dl><dt>Название </dt><dd><input type="text" name="name" required></dd></dl>
            <dl><dt>Изображение </dt><dd><input type="file" accept="image/jpeg,image/png" name="picture"></dd></dl>
            <dl><dt>Ссылка </dt><dd><input type="text" name="link"></dd></dl>
            <dl><dt>Описание </dt><dd><textarea rows="10" cols="45" name="description"></textarea></dd></dl>
            <!--категория-->
            <dl><dt>Категория </dt><dd><select required size="1" name="category">
                <option disabled>Выберите категорию</option>
                <option selected value="3">Фильмы</option>
                <option value="4">Сериалы</option>
                <option value="5">Игры</option>
                <option value="6">Музыка</option>
                <option value="7">Мультфильмы</option>
                <option value="8">Другое</option>
                </select>
                </dd></dl>
            <input type="submit" value="Создать">
        </form>
    </div>

@endsection