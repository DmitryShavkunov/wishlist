
@extends ('layout')

@section ('content')
    <div class="page-header">
        <h1>
            Список желаемого
        </h1>
    </div>
    <div>
        <a href="/shavy_project/public/desired_item/create_item.php">Добавить новый предмет</a>
    </div>
    <div>
        <dl><dt>Категория </dt><dd><select required size="1" name="category">
                    <option disabled>Выберите категорию</option>
                    <option selected value="0">Все</option>
                    <option value="3">Фильмы</option>
                    <option value="4">Сериалы</option>
                    <option value="5">Игры</option>
                    <option value="6">Музыка</option>
                    <option value="7">Мультфильмы</option>
                    <option value="8">Другое</option>
                </select>
            </dd></dl>
    </div>
    <div class="row">
        <ul>
            @foreach ( $items as $item )
                <dl>
                    <dt><img src="/shavy_project/public{{$item->picture }}" width="200" alt="{{$item->name }}"></dt>
                    <dd><p>id: {{$item->id}}</p>
                        @if($item->link)
                            <p>Название:<a href="{{$item->link }}">{{$item->name }}</a></p>
                        @else
                            <p>Название:{{$item->name }}</p>
                        @endif
                        <p>Дата и время создания: {{$item->created_at}}</p>
                        <p>Описание: {{$item->description}}</p>
                        <p>Категория: {{$item->category}}</p>
                    </dd>
                </dl>

            @endforeach
        </ul>
    </div>

    <div class="panel-group" id="accordion">
        @foreach ( $categorys as $category )
        <div class="panel panel-default">
            <!-- Заголовок 1 панели -->
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">{{$category->name }}</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <!-- Содержимое 1 панели -->
                <div class="panel-body">
                    @foreach ( $items as $item )
                        @if($item->id_category==$category->id)
                            <dl>
                                <dt><img src="/shavy_project/public{{$item->picture }}" width="200" alt="{{$item->name }}"></dt>
                                <dd><p>id: {{$item->id}}</p>
                                    @if($item->link)
                                        <p>Название:<a href="{{$item->link }}">{{$item->name }}</a></p>
                                    @else
                                        <p>Название:{{$item->name }}</p>
                                    @endif
                                    <p>Описание: {{$item->description}}</p>
                                </dd>
                            </dl>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection